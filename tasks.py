# --8<-- [start:imports]
import dataclasses
from typing import Callable, Generic, List, Optional, TypeVar
# --8<-- [end:imports]


# --8<-- [start:cached]
def cached(f):
    cache = {}

    def wrapper(*args):        
        key = tuple(args)

        if key in cache:
            return cache[key]
        
        elif len(cache) >= 3:
            oldest_key = next(iter(cache))
            del cache[oldest_key]
        
        result = f(*args)
        cache[key] = result
        return result
            
    return wrapper
# --8<-- [end:cached]


# --8<-- [start:TypeVar]
T = TypeVar("T")
# --8<-- [end:TypeVar]

# --8<-- [start:ParseResult]
@dataclasses.dataclass
class ParseResult(Generic[T]):
    value: Optional[T]
    rest: str

    @staticmethod
    def invalid(rest: str) -> "ParseResult":
        return ParseResult(value=None, rest=rest)

    def is_valid(self) -> bool:
        return self.value is not None
# --8<-- [end:ParseResult]

# --8<-- [start:Parser]
Parser = Callable[[str], ParseResult[T]]
# --8<-- [end:Parser]

# --8<-- [start:parser_char]
def parser_char(char: str) -> Parser[str]:
    if len(char) > 1 or not char:
        raise ValueError()

    def parse(input: str):

        if input and input[0] == char:
            return ParseResult(value=char, rest=input[len(char):])
        
        return ParseResult.invalid(input)
        
    return parse
# --8<-- [end:parser_char]


# --8<-- [start:parser_repeat]
def parser_repeat(parser: Parser[T]) -> Parser[List[T]]:
    def repeat(input: str):
        values = []
        rest = input
        result = parser(rest)

        while result.is_valid():
            values.append(result.value)
            rest = result.rest
            result = parser(rest)
        
        return ParseResult(value=values, rest=rest)
    
    return repeat
# --8<-- [end:parser_repeat]

    
# --8<-- [start:parser_seq]
def parser_seq(parsers: List[Parser]) -> Parser:
    def seq(input: str):
        values = []
        rest = input
        
        for parser in parsers:
            result = parser(rest)
            values.append(result.value)
            rest = result.rest
            
            if not result.is_valid():
                return ParseResult.invalid(input)
        
        return ParseResult(values, rest)
    
    return seq
# --8<-- [end:parser_seq]


# --8<-- [start:parser_choice]
def parser_choice(parsers: List[Parser]) -> Parser:
    def anyof(input: str):
        for parser in parsers:
            result = parser(input)
            
            if result.is_valid():
                return result
        
        return ParseResult.invalid(input)

    return anyof
# --8<-- [end:parser_choice]




# --8<-- [start:R]
R = TypeVar("R")
# --8<-- [end:R]


# --8<-- [start:parser_map]
def parser_map(parser: Parser[R], map_fn: Callable[[R], Optional[T]]) -> Parser[T]:
    def ma(input: str):
        result = parser(input)

        if result.is_valid():
            map_result = map_fn(result.value)

            if map_result is None:
                return ParseResult.invalid(input)

            return ParseResult(map_result, result.rest)

        return ParseResult.invalid(input)

    return ma
# --8<-- [end:parser_map]


# --8<-- [start:parser_matches]
def parser_matches(filter_fn: Callable[[str], bool]) -> Parser[str]:
    def match(input: str):
        if not input:
            return ParseResult.invalid(input) 

        if filter_fn(input[0]):
            return ParseResult(input[0], input[1:])
        
        return ParseResult.invalid(input) 
    
    return match
# --8<-- [end:parser_matches]


# --8<-- [start:parser_string]
def parser_string(string: str) -> Parser[str]:
    def parse(input: str):
        letters = list(string)
        parsers = []

        if not string:
            return ParseResult("", input)

        for c in letters:
            parsers.append(parser_char(c))

        result = parser_seq(parsers)
        result = result(input) 
        
        if result.value:
            result.value = ''.join(result.value)

        return ParseResult(result.value, result.rest)

    return parse
# --8<-- [end:parser_string]


# --8<-- [start:parser_int]
def parser_int() -> Parser[int]:
    def parse(input: str):

        parser = parser_matches(lambda x: x in ("0123456789"))
        rest = input 
        number = ''

        for c in input:
            result = parser(rest)

            if not result.is_valid():
                break

            rest = result.rest
            number += result.value
            
        try:
            result.value = int(number)
            return result        
        except:
            return result
    
    return parse
# --8<-- [end:parser_int]
